// Copyright 2021 Jonas Weber
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cache

import (
	"context"
	"errors"
	"io"

	digest "github.com/opencontainers/go-digest"
)

// ErrWrongState is returned when the entry writer is in the wrong
// state, for example the user attempts to discard an already
// committed writer.
var ErrWrongState = errors.New("wrong state of cache writer")

// EntryWriter is used to control the actual submission of a blob
// to a cache. The following protocol is to be used when an EntryWriter
// is returned:
//
// * Write() to the io.Writer part.
// * Close() the file via io.Closer.
// After closing, the digest can be retrieved via Digest(), the written size
// via Size().
//
// If the entry is to be committed into the store, use Commit(), otherwise, use
// Discard().
//
// For cases where the final call to Commit() or Discard() is missing, the cache
// SHOULD offer a way to discard accrued garbage.
type EntryWriter interface {
	io.Writer
	io.Closer

	Digest() digest.Digest
	Size() int64
	Commit() error
	Discard() error

	SetMeta(context.Context, string, []byte) error
}

// A Cache stores blobs and makes them available using their digest.
type Cache interface {
	IsCached(context.Context, digest.Digest) (bool, error)

	Open(context.Context, digest.Digest) (io.ReadCloser, int64, error)
	Create(context.Context) (EntryWriter, error)

	ReadMeta(context.Context, digest.Digest, string) ([]byte, error)
}
