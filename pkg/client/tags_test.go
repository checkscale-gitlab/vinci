// Copyright 2021 Jonas Weber
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package client_test

import (
	"context"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/thriqon/vinci/pkg/client"
)

func TestTagsList_RepositoryMissing(t *testing.T) {
	t.Parallel()

	dbc := newTestCache(t)
	mux := http.NewServeMux()

	mux.HandleFunc("/v2/image/a/tags/list", func(w http.ResponseWriter, r *http.Request) {
		http.Error(w, "Not Found", http.StatusNotFound)
	})

	mux.HandleFunc("/v2/", func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, http.MethodGet, r.Method)
	})

	server := httptest.NewTLSServer(mux)
	defer server.Close()

	// when
	cl := client.NewClient(dbc, nil, server.Client().Transport)

	in, err := client.ParseImageName(server.Listener.Addr().String() + "/image/a")
	require.NoError(t, err)

	// then
	tags, err := cl.Tags.List(context.Background(), in)
	require.NoError(t, err)
	require.Equal(t, tags, []string{})
}

func limitByValues(vals []string, q url.Values) []string {
	if last := q.Get("last"); last != "" {
		for i, s := range vals {
			if s == last {
				vals = vals[i+1:]
			}
		}
	}

	if n, err := strconv.Atoi(q.Get("n")); err == nil {
		if n > len(vals) {
			n = len(vals)
		}

		vals = vals[0:n]
	}

	return vals
}

func TestTagsList(t *testing.T) {
	t.Parallel()

	dbc := newTestCache(t)
	mux := http.NewServeMux()

	mux.HandleFunc("/v2/image/a/tags/list", func(w http.ResponseWriter, r *http.Request) {
		tags := []string{"a", "b", "latest"}
		q := r.URL.Query()

		tags = limitByValues(tags, q)

		data := struct {
			Name string   `json:"name"`
			Tags []string `json:"tags"`
		}{
			Name: "image/a",
			Tags: tags,
		}

		require.NoError(t, json.NewEncoder(w).Encode(&data))
	})

	mux.HandleFunc("/v2/", func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, http.MethodGet, r.Method)
	})

	server := httptest.NewTLSServer(mux)

	defer server.Close()

	// when
	cl := client.NewClient(dbc, nil, server.Client().Transport)

	in, err := client.ParseImageName(server.Listener.Addr().String() + "/image/a")
	require.NoError(t, err)

	// then
	tags, err := cl.Tags.List(context.Background(), in)
	require.NoError(t, err)
	assert.ElementsMatch(t, []string{"a", "b", "latest"}, tags)
}
