// Copyright 2021 Jonas Weber
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package client

import (
	"context"
	"errors"
	"fmt"
	"net/http"

	digest "github.com/opencontainers/go-digest"
	"gitlab.com/thriqon/vinci/pkg/cache"
)

type requestOption func(ctx context.Context, req *http.Request, received *digest.Digest, cw cache.EntryWriter) error

func accept(val string) requestOption {
	return func(_ context.Context, r *http.Request, _ *digest.Digest, _ cache.EntryWriter) error {
		if r != nil {
			r.Header.Set("Accept", val)
		}

		return nil
	}
}

func contentType(val string) requestOption {
	return func(_ context.Context, r *http.Request, _ *digest.Digest, _ cache.EntryWriter) error {
		if r != nil {
			r.Header.Set("Content-Type", val)
		}

		return nil
	}
}

var ErrUnexpectedDigest = errors.New("unexpected digest")

func expectDigest(val digest.Digest) requestOption {
	return func(_ context.Context, _ *http.Request, actual *digest.Digest, _ cache.EntryWriter) error {
		if actual != nil && *actual != val {
			return fmt.Errorf("unable to validate receipt of %s, got %s: %w", val, actual, ErrUnexpectedDigest)
		}

		return nil
	}
}

func Metadata(key string, val []byte) requestOption { //nolint:golint // Only used for options, not annoying.
	return func(ctx context.Context, _ *http.Request, _ *digest.Digest, cw cache.EntryWriter) error {
		if cw != nil {
			if err := cw.SetMeta(ctx, key, val); err != nil {
				return fmt.Errorf("set %s=%v: %w", key, val, err)
			}
		}

		return nil
	}
}
