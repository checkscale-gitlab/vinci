// Copyright 2021 Jonas Weber
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package client

import (
	"context"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"

	"github.com/google/go-containerregistry/pkg/v1/remote/transport"
	hclog "github.com/hashicorp/go-hclog"
	digest "github.com/opencontainers/go-digest"
)

// MetaRepository is the name of the meta property this blob was retrieved from.
const MetaRepository = "repository"

var ErrRegistryUnwilling = errors.New("registry did not accept blob upload request")

type BlobService struct {
	client *Client
}

func (bs *BlobService) Head(ctx context.Context, n ImageName, dgst digest.Digest) error { //nolint:interfacer // We want to document the expected data type.
	cl, err := bs.client.httpClientFor(n, transport.PullScope)
	if err != nil {
		return fmt.Errorf("unable to get client for %s: %w", n, err)
	}
	u := n.AsURLPrefix() + "/blobs/" + dgst.String()

	req, err := http.NewRequestWithContext(ctx, http.MethodHead, u, nil)
	if err != nil {
		return fmt.Errorf("unable to prepare head for %s: %w", n, err)
	}

	res, err := cl.Do(req)
	if err != nil {
		return fmt.Errorf("unable to execute head for %s: %w", n, err)
	}

	defer res.Body.Close()

	_, err = io.Copy(ioutil.Discard, res.Body)
	if err != nil {
		return fmt.Errorf("unable to discard HTTP body: %w", err)
	}

	if res.StatusCode != http.StatusOK {
		return fmt.Errorf("did not receive 200: %w", ErrUnexpectedStatusCode)
	}

	return nil
}

func (bs *BlobService) Get(ctx context.Context, n ImageName, dgst digest.Digest, opts ...requestOption) error {
	cl, err := bs.client.httpClientFor(n, transport.PullScope)
	if err != nil {
		return fmt.Errorf("unable to get client for %s: %w", n, err)
	}
	u := n.AsURLPrefix() + "/blobs/" + dgst.String()

	opts = append(opts,
		expectDigest(dgst),
		Metadata(MetaRepository, []byte(n.String())))

	_, err = bs.client.getToCache(ctx, cl, u, opts...)

	return err
}

func (bs *BlobService) Put(ctx context.Context, n ImageName, dgst digest.Digest) error {
	logger := hclog.FromContext(ctx)

	cl, err := bs.client.httpClientFor(n, transport.PushScope)
	if err != nil {
		return fmt.Errorf("unable to get client for %s: %w", n, err)
	}

	var queryParams string
	knownSource, err := bs.client.blobCache.ReadMeta(ctx, dgst, MetaRepository)
	if err == nil {
		mounted, err := ParseImageName(string(knownSource))
		if err != nil {
			return fmt.Errorf("stored known source: %w", err)
		}

		if mounted.Host == n.Host {
			logger.Info("try to mount blob", "source", string(knownSource))

			queryParams = "?mount=" + dgst.String() + "&from=" + string(knownSource)
		} else {
			logger.Trace("reject known source", "source", string(knownSource), "host", n.Host)
		}
	} else {
		logger.Trace("meta repository name error", "error", err)
	}

	req, err := http.NewRequestWithContext(ctx, http.MethodPost, n.AsURLPrefix()+"/blobs/uploads/"+queryParams, nil)
	if err != nil {
		return fmt.Errorf("unable to prepare POST to initiate upload: %w", err)
	}

	res, err := cl.Do(req)
	if err != nil {
		return fmt.Errorf("unable to POST to initiate upload: %w", err)
	}

	defer res.Body.Close()

	if res.StatusCode == http.StatusCreated {
		logger.Info("mount successful")

		return nil
	}

	if res.StatusCode != http.StatusAccepted {
		return fmt.Errorf("unable to get session url for blob upload with status %s: %w", res.Status, ErrRegistryUnwilling)
	}

	sessionURL, err := res.Location()
	if err != nil {
		return fmt.Errorf("unable to get session url for blob upload, registry sent none back: %w", err)
	}

	q := sessionURL.Query()
	q.Set("digest", dgst.String())
	sessionURL.RawQuery = q.Encode()

	return bs.client.uploadFromCache(ctx, cl, sessionURL.String(), dgst,
		contentType("application/octet-stream"))
}
