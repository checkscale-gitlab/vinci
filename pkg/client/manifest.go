// Copyright 2021 Jonas Weber
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package client

import (
	"context"
	"fmt"
	"net/http"

	"github.com/google/go-containerregistry/pkg/v1/remote/transport"
	digest "github.com/opencontainers/go-digest"
)

type ManifestService struct {
	client *Client
}

func (ms *ManifestService) Get(ctx context.Context, mr ManifestRef, opts ...requestOption) (*digest.Digest, error) {
	cl, err := ms.client.httpClientFor(mr.ImageName, transport.PullScope)
	if err != nil {
		return nil, fmt.Errorf("unable to get client for %s: %w", mr, err)
	}

	opts = append(opts, accept("application/vnd.docker.distribution.manifest.v2+json"))

	if mr.HasDigest() {
		opts = append(opts, expectDigest(mr.Digest))
	}

	return ms.client.getToCache(ctx, cl, mr.ManifestURL(), opts...)
}

func (ms *ManifestService) Head(ctx context.Context, mr ManifestRef) error {
	cl, err := ms.client.httpClientFor(mr.ImageName, transport.PullScope)
	if err != nil {
		return fmt.Errorf("unable to get client for %s: %w", mr, err)
	}

	req, err := http.NewRequestWithContext(ctx, http.MethodHead, mr.ManifestURL(), nil)
	if err != nil {
		return fmt.Errorf("unable to prepare head to manifest: %w", err)
	}

	req.Header.Set("Accept", "application/vnd.docker.distribution.manifest.v2+json")

	res, err := cl.Do(req)
	if err != nil {
		return fmt.Errorf("unable to send head to manifest: %w", err)
	}

	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		return fmt.Errorf("manifest unavailable, status %s: %w", res.Status, ErrUnexpectedStatusCode)
	}

	return nil
}

func (ms *ManifestService) Put(ctx context.Context, n ImageName, mr ManifestRef, dgst digest.Digest) error {
	cl, err := ms.client.httpClientFor(n, transport.PushScope)
	if err != nil {
		return fmt.Errorf("unable to get client for %s: %w", n, err)
	}
	u := mr.ManifestURL()

	return ms.client.uploadFromCache(ctx, cl, u, dgst, contentType("application/vnd.docker.distribution.manifest.v2+json"))
}
