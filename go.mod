module gitlab.com/thriqon/vinci

go 1.16

require (
	github.com/fatih/color v1.10.0 // indirect
	github.com/google/go-cmp v0.5.5 // indirect
	github.com/google/go-containerregistry v0.1.3
	github.com/hashicorp/go-hclog v0.14.1
	github.com/mitchellh/hashstructure v1.0.0
	github.com/opencontainers/go-digest v1.0.0
	github.com/opencontainers/image-spec v1.0.1
	github.com/spf13/cobra v1.1.1
	github.com/stretchr/testify v1.7.0
	golang.org/x/sync v0.0.0-20201207232520-09787c993a3a
	golang.org/x/sys v0.0.0-20210320140829-1e4c9ba3b0c4 // indirect
	gopkg.in/yaml.v2 v2.4.0
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776
)
